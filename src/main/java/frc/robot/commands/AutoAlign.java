/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class AutoAlign extends CommandBase {
  
  NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight"); 
  NetworkTableEntry tx = table.getEntry("tx");
  NetworkTableEntry ty = table.getEntry("ty");
  NetworkTableEntry ta = table.getEntry("ta");

  private final DriveTrain m_drivetrain;

  private final double KpAim = 0.03; //0.05
  private final double KpDistance = 0.3;
  private final double min_command = 0.1;
   double steering_adjust;

  public AutoAlign(DriveTrain drivetrain) {
    m_drivetrain = drivetrain;

    addRequirements(drivetrain);  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
      steering_adjust = 0;

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double heading_error = tx.getDouble(0.0);
    double distance_error = -1* ty.getDouble(0.0);
    if(heading_error > 10){
      steering_adjust = KpAim*heading_error - min_command;
    }else if(heading_error < -10){
      steering_adjust = KpAim*heading_error + min_command;
    }else if(heading_error > 0){
      steering_adjust = KpAim*heading_error + min_command;
    }else{
      steering_adjust = KpAim*heading_error - min_command;
    }

   // steering_adjust /= 3;
    double distance_adjust =  KpDistance * distance_error;

    m_drivetrain.tankDrive(steering_adjust + distance_adjust, -1*steering_adjust + distance_adjust);
    SmartDashboard.putNumber("Auto Align Left Power: ", steering_adjust + distance_adjust);
    SmartDashboard.putNumber("Auto Align Right Power: ", -1*steering_adjust + distance_adjust);
    SmartDashboard.putNumber("tx: ", heading_error);
    SmartDashboard.putNumber("distance_adjust: ", distance_adjust);
    
  }


  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
