/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.commands.*;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.ColorSensor;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

;
/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DriveTrain driveTrain = new DriveTrain();
  private final ColorSensor colorSensor = new ColorSensor();

  private final AutonomousDrive m_autoCommand = new AutonomousDrive(driveTrain);
  private final AutoAlign m_autoalign = new AutoAlign(driveTrain);
  private final SensingColor m_SensingColor = new SensingColor(colorSensor);

 

  public static Joystick joystick = new Joystick(1);
  
  private final JoystickButton joystickButtonAlign = new JoystickButton(joystick, 1);
  private final JoystickButton joystickSideButton = new JoystickButton(joystick, 2);






  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {

    driveTrain.setDefaultCommand(new Drive(driveTrain));

    colorSensor.setDefaultCommand(new SensingColor(colorSensor));
    
    // Configure the button bindings
    configureButtonBindings();

   // m_exampleSubsystem.setDefaultCommand(new ExampleCommand(m_exampleSubsystem));
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() 
  {
    joystickButtonAlign.whenHeld(m_autoalign);
   // joystickButton.whenPressed(new toggleSpeed(m_exampleSubsystem, 1));
   // joystickButtonS.whenPressed(new toggleSpeed(m_exampleSubsystem, 2));
  }


  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return m_autoCommand;
  }
}
