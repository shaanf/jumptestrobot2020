/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotContainer;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class DriveTrain extends SubsystemBase {
  /**
   * Creates a new ExampleSubsystem.
   */

   //Motors
  public PWMVictorSPX m_motorLF = new PWMVictorSPX(0);
  public PWMVictorSPX m_motorLB = new PWMVictorSPX(1);
  public PWMVictorSPX m_motorRF = new PWMVictorSPX(2);
  public PWMVictorSPX m_motorRB = new PWMVictorSPX(3);

  public boolean halfSpeed = false;
  public boolean ThirdSpeed = false;


  //SpeedController

  public SpeedControllerGroup m_SpeedControllerGroupL = new SpeedControllerGroup(m_motorLB, m_motorLF);
  public SpeedControllerGroup m_SpeedControllerGroupR = new SpeedControllerGroup(m_motorRB, m_motorRF);

  public DifferentialDrive m_DifferentialDrive = new DifferentialDrive(m_SpeedControllerGroupL, m_SpeedControllerGroupR);



  public DriveTrain() 
  {

  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
   
  }
  public void tankDrive(Double left,Double right)
  {
    m_DifferentialDrive.tankDrive(left, right);
  }
  public void arcadeDrive(Double left, Double right)
  {
    m_DifferentialDrive.arcadeDrive(left, right);
  }

  
}

